from django.contrib.auth.forms import UserCreationForm as CustomUserCreationForm
from django.contrib.auth.forms import UserChangeForm as CustomUserChangeForm

from accounts.models import User


class UserCreationForm(CustomUserCreationForm):
    class Meta:
        model = User
        fields = ("email",)


class UserChangeForm(CustomUserChangeForm):
    class Meta:
        model = User
        fields = ("email",)
