from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as CustomUserAdmin
from accounts.forms import UserChangeForm, UserCreationForm
from accounts.models import User

# Register your models here.
class UserAdmin(CustomUserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = User
    list_display = ["email"]


admin.site.register(User, UserAdmin)
