"""
ASGI config for config project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/dev/howto/deployment/asgi/
"""

import os
from django.apps import apps
from django.conf import settings

from django.core.asgi import get_asgi_application
from fastapi import FastAPI

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')
apps.populate(settings.INSTALLED_APPS)

application = FastAPI(title='oStack', debug=settings.DEBUG)
application.mount('', get_asgi_application())
