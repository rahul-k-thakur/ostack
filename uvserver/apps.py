from django.apps import AppConfig


class UvserverConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "uvserver"
