from datetime import datetime
import sys
from typing import Any, Optional

from django.conf import settings

# from django.core.management.base import BaseCommand

from django.core.management.commands.runserver import Command as RunserverCommand

import uvicorn


class Command(RunserverCommand):
    help = "Starts a uvicorn web server for development"

    def handle(self, *args: Any, **options: Any) -> Optional[str]:
        quit_command = "CTRL-BREAK" if sys.platform == "win32" else "CONTROL-C"
        now = datetime.now().strftime("%B %d, %Y - %X")
        self.default_port = 8000
        self.stdout.write(now)
        self.stdout.write(
            ("Django version %(version)s, using settings %(settings)r\n" "Starting development server at %(protocol)s://%(addr)s:%(port)s/\n" "Quit the server with %(quit_command)s.")
            % {
                "version": self.get_version(),
                "settings": settings.SETTINGS_MODULE,
                "protocol": self.protocol,
                "addr": self.default_addr,
                "port": self.default_port,
                "quit_command": quit_command,
            }
        )
        uvicorn.run("config.asgi:application", host=self.default_addr, port=self.default_port, log_level="trace", reload=True)
